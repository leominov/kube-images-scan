# kube-images-scan

Поиск образов в кластере.

## Usage

```
Usage of ./kube-images-scan:
  -exclude string
    	Filter to exclude images (default "qlean-242314")
  -filter string
    	Filter for images
  -ns string
    	Namespace selector (default ".*")
```

## Примеры

### Все образы в определенном namespace

```shell
./kube-images-scan --ns default
```

### Образы из определенного registry

```shell
./kube-images-scan --filter registry.manage.cloud.qlean.ru
```
