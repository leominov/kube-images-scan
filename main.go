package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"regexp"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	namespaceSelector = flag.String("ns", ".*", "Namespace selector")
	imageNameFilter   = flag.String("filter", "", "Filter for images")
	imageNameInclude  = flag.String("include", "", "Include only specific images")
	imageNameExclude  = flag.String("exclude", "qlean-242314", "Filter to exclude images")
)

func realMain() error {
	flag.Parse()

	namespaceSelectorRE, err := regexp.Compile(*namespaceSelector)
	if err != nil {
		return err
	}

	imageNameExcludeRE, err := regexp.Compile(*imageNameExclude)
	if err != nil {
		return err
	}

	imageNameIncludeRE, err := regexp.Compile(*imageNameInclude)
	if err != nil {
		return err
	}

	imageNameFilterRE, err := regexp.Compile(*imageNameFilter)
	if err != nil {
		return err
	}

	cli, err := NewClientSet()
	if err != nil {
		return err
	}

	namespaceInterface := cli.CoreV1().Namespaces()
	namespaceList, err := namespaceInterface.List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return err
	}

	for _, namespace := range namespaceList.Items {
		if !namespaceSelectorRE.MatchString(namespace.Name) {
			continue
		}
		podInterface := cli.CoreV1().Pods(namespace.Name)
		podList, err := getPodsInChunks(podInterface, metav1.ListOptions{})
		if err != nil {
			return err
		}
		if len(podList.Items) == 0 {
			continue
		}

		var containers []string
		for _, pod := range podList.Items {
			if pod.Status.Phase != corev1.PodRunning {
				continue
			}
			podContainers := pod.Spec.Containers
			podContainers = append(podContainers, pod.Spec.InitContainers...)
			for _, container := range podContainers {
				if imageNameIncludeRE != nil {
					if !imageNameIncludeRE.MatchString(container.Image) {
						continue
					}
				}
				if imageNameExcludeRE.MatchString(container.Image) {
					continue
				}
				if !imageNameFilterRE.MatchString(container.Image) {
					continue
				}
				item := fmt.Sprintf("%s (%s/%s)", container.Image, pod.Name, container.Name)
				containers = append(containers, item)
			}
		}
		if len(containers) == 0 {
			continue
		}

		fmt.Println(namespace.Name)
		for _, container := range containers {
			fmt.Printf("  * %s\n", container)
		}
	}

	return nil
}

func main() {
	err := realMain()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
